<!DOCTYPE html>
<html>
  <head>
    <title>Konto</title>
    <meta charset="UTF-8">
    <link href="mainstyles.css" type="text/css" rel="stylesheet"/>
    <style>

    </style>
      <script>
    function move_page(a){
      location.href = a;
    }
  </script>
</head>
<body>

<div id="main_div"> 
  <div style="width: 1200px; margin: 0 auto" >
    <button>Strona główna</button>
    <?php
    session_cache_limiter('');
    session_start();
      if (!isset($_SESSION['login'])) {
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">Konto</button>';
      } else {
          echo '<button id="wyloguj" onclick="move_page'."('Konto.php')".'">Wyloguj</button>';
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">'.$_SESSION['login'].'</button>';
      }
    ?>

    <br>
    <img class="img1" src="images/Baner.png">
    <br>
    <button class="mainbtts" onclick="move_page('kursy.php')" id="kursy" >Kursy</button>
    <button class="mainbtts" onclick="move_page('kursanci.php')" id="kursanci" >Kursanci</button>
    <button class="mainbtts" onclick="move_page('forum.php')" id="forum" >Forum</button>
    <br><br><br><br>
    
    <div style="width: 599px; float:left; border-right:1px solid black; height:560px">
        <div style="float:right; margin-right:10px;">
            <p class="d" style=" font-size:24px; float:right;"><span>Rejestracja</span></p><br><br>

            <form method="POST" style="position: relative; top: 8px" action="Konto_add.php">
              <div>
            <p ><span style="float:right; font-size:14px">Podaj nazwę:</span></p>
            <input required pattern="[A-Za-z0-9]{5,}" title="Min 5 znaków" class="konto" type="text" name="Nazwa" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Podaj hasło:</span></p>
            <input required pattern="[A-Za-z0-9]{7,}" title="Min 7 znaków" class="konto" type="password" name="Haslo" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Powtórz hasło:</span></p>
            <input required pattern="[A-Za-z0-9]{7,}" title="Powtórz poprawnie hasło" class="konto" type="password" name="Powt" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Podaj swoje imię:</span></p>
            <input required pattern="[A-Za-z]{2,}" title="Min 5 znaków" class="konto" type="text" name="imie" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Podaj swoje nazwisko:</span></p>
            <input required pattern="[A-Za-z]{2,}" title="Min 5 znaków" class="konto" type="text" name="nazwisko" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Podaj nr telefonu:</span></p>
            <input required pattern="[0-9+]{3,}" title="Three letter country code" class="konto" type="text" name="Telefon" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Podaj adres e-mail:</span></p>
            <input required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}" title="Three letter country code" class="konto" type="email" name="Mail" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
            <p ><span style="float:right; font-size:14px">Zarejestruj jako:</span></p><br>
              </div>
            <select name="typ" style="font-size:13px; border: 1px;  border-style: groove; border-color:#666666; height:20px; width:324px; margin-top:8px; margin-left:0px; margin-right:0px">
              <option value="u">Użytkownik</option>
              <option value="k">Kursant</option>
            </select><br>
            <p style="font-size:16px; float: right; position:relative; top:5px"><span>Regulamin</span></p><br><br>
            <p style="float: right;  position:relative; top:-2px"><input required type="checkbox" ><span style="font-family: 'Arial'; font-size:13px">Akceptuje regulamin</span></p><br><br>
            <br>
            <input type="submit" name="regbtn" style="float: right; font-size:24px; font-family:'Yu Gothic UI'; font-weight:400; margin-top: -8px" value="Zarejestruj">
            </form>
        </div>
    </div>

    <div style="width: 500px; float:left;">
      <div style="float:left; margin-left:10px;"> 
        <p class="d" style="font-size:24px; float:left;"><span>Logowanie</span></p><br><br>

        <form method="POST" style="position: relative; top: 8px" action="Konto_check.php">
          <p ><span style="float:left; font-size:14px">Podaj nazwę:</span></p><br>
          <input required class="konto" type="text" name="Nazwa_log" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
          <p ><span style="float:left; font-size:14px">Podaj hasło:</span></p><br>
          <input required class="konto" type="text" name="Haslo_log" style="border: 1px;  border-style: groove; border-color:#666666; width:320px; margin-top:8px;">
          <p ><span style="float:left; font-size:14px">Zaloguj jako:</span></p><br>
          <select style="font-size:13px; border: 1px;  border-style: groove; border-color:#666666; height:20px; width:324px; margin-top:8px; margin-left:0px; margin-right:0px">
            <option value="u">Użytkownik</option>
            <option value="k">Kursant</option>
          </select><br>
          <p style="float: left;  position:relative; top:5px"><input type="checkbox" ><span style="font-family: 'Arial'; font-size:13px">Zapamiętaj mnie</span></p><br><br>
          <input type="submit" style="float: left; font-size:24px; font-family:'Yu Gothic UI'; font-weight:400; position:relative; top:10px" value="Zaloguj">
        </form>

      </div>
    </div>
  </div><!-- Strona -->
</div><!-- main_div -->
</body>