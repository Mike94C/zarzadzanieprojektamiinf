<!DOCTYPE html>
<html>
  <head>
    <title>Dodaj_kurs</title>
    <meta charset="UTF-8">
    <link href="mainstyles.css" type="text/css" rel="stylesheet"/>
    <style>

    </style>
    <script>
        var kalend;
        var hid;
        var it;
        function dodaj_dzien(){
            kalend = document.getElementById("kalend");
            hid = document.getElementById("hid");
            it = 0;
        }

        function nowy_dzien(){
            kalend.insertAdjacentHTML ('beforebegin', '<span style="font-size: 14px">Dzień</span>  <input required type="date" id="dzien" name="dzien_'+it+'" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:20px; width:200px; margin-top:8px; margin-bottom:1px"> <span style="font-size: 14px">Godzina od</span>  <input required type="time" name="godz_p_'+it+'" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:20px; width:200px; margin-top:8px; margin-bottom:1px">  <span style="font-size: 14px">do</span>  <input required type="time" name="godz_z_'+it+'" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:20px; width:200px; margin-top:8px; margin-bottom:1px">  <span style="font-size: 14px">Maks. punktów</span>  <input required type="number" name="max_pkt_'+it+'" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:20px; width:200px; margin-top:8px; margin-bottom:1px"><br>');
            it += 1;
            hid.value = it;
            
        }

        function dodaj_dane(){
            
        }

    function move_page(a){
      location.href = a;
    }

    </script>
</head>
<body onload="dodaj_dzien()">

<div id="main_div"> 
  <div style="width: 1200px; margin: 0 auto" >
    <button>Strona główna</button>
    <?php
    session_cache_limiter('');
    session_start();
      if (!isset($_SESSION['login'])) {
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">Konto</button>';
      } else {
          echo '<button id="wyloguj" onclick="move_page'."('Konto.php')".'">Wyloguj</button>';
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">'.$_SESSION['login'].'</button>';
      }
    ?>

    <br>
    <img class="img1" src="images/Baner.png">
    <br>
    <button class="mainbtts" onclick="move_page('kursy.php')" id="kursy" >Kursy</button>
    <button class="mainbtts" onclick="move_page('kursanci.php')" id="kursanci" >Kursanci</button>
    <button class="mainbtts" onclick="move_page('forum.php')" id="forum" >Forum</button>
    <br><br><br><br>
    
    
    <div style="width:1200px">
        <h3 style="font-size: 24px">Utwórz nowy kurs</h3><br>
        <form method="post" action="Kurs_add.php">
            <div style="width: 1200px">
            <div style="float: left; width: 160px">
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Nazwa kursu: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e"style="float: right">Rodzaj kursu: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Rozpoczęcie: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Zakończenie: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Liczba Godzin: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Lokalizacja: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Cena: </span></p><br>
                <p class="e" style="margin-right:12px; margin-top:10px; margin-bottom: 15px; "><span class="e" style="float: right">Certyfikacja: </span></p><br>
            </div>
            <div style="float:left; ">
                <input required type="text" name="kurs_nazwa" class="s" ><br>
                <select required type="text" name="kurs_typ" style="font-size:13px; border: 1px;  border-style: groove; border-color:#666666; height:22px; width:324px; margin-bottom:1px">
                    <option value="">(wybierz)</option>
                    <option value="p">Programowanie</option>
                    <option value="g">Grafika</option>
                    <option value="s">Obsługa urządzeń sieci</option>
                </select><br>
                <input required type="date" name="kurs_rozp" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:22px; width:320px; margin-top:8px; margin-bottom:1px"><br>
                <input required type="date" name="kurs_zako" style="border: 1px; font-size:13px; border-style: groove; border-color:#666666; height:22px; width:320px; margin-top:8px; margin-bottom:1px"><br>
                <input required type="number" name="kurs_licz" class="sq"><br>
                <input required type="text" name="kurs_loka" class="sq"><br>
                <input required type="number" name="kurs_cena" class="sq"><br>
                <input required type="text" name="kurs_cert" class="sq"><br>
            </div>
            </div>
            <div style="left: 10px; margin-left: 10px; float:left">
                <p class="e"><span>Opis:</span></p>
                <textarea required name="Tresc" style="resize: none; width: 650px; height: 230px; font-family:'Arial'"></textarea>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div style="width: 1200px; ">
            <hr style="width: 1200px; margin-bottom: 0px">
            <div style="position: relative; top: 0px; width: 1200px; ">
                <h3 style="font-size: 18px; margin-top: 0px"><span>Kalendarium</span></h3>
                
                <div id="kalend">
                </div>
                <button style="font-size: 14px" onclick="nowy_dzien()">Dodaj nowy dzień</button>
            </div>
            <div style="width:1200px; margin: 0 auto; ">
            <br><br>
            <input type="hidden" id="hid" name="num">
                <input type="submit" value="Zatwierdź" style="font-size:24px; font-family:'Yu Gothic UI'; font-weight:400; display: block; margin:0 auto; width:200px">
            </div>
            </div>
        </form>
    </div>
    </div><!-- Strona -->
</div><!-- main_div -->
</body>