<!DOCTYPE html>
<html>
  <head>
    <title>Strona glowna</title>
    <meta charset="UTF-8">
    <link href="mainstyles.css" type="text/css" rel="stylesheet"/>
    <style>
    #login {visibility: hidden; border-right: 1px solid #333 }
    </style>
    <script>

    </script>
      <script>
    function move_page(a){
      location.href = a;
    }
  </script>
</head>
<body>

<div id="main_div"> 
  <div style="width: 1200px; margin: 0 auto" >
    <button>Strona główna</button>
    <?php
    session_cache_limiter('');
    session_start();
      if (!isset($_SESSION['login'])) {
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">Konto</button>';
      } else {
          echo '<button id="wyloguj" onclick="move_page'."('Konto.php')".'">Wyloguj</button>';
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">'.$_SESSION['login'].'</button>';
      }
    ?>

    <br>
    <img class="img1" src="images/Baner.png">
    <br>
    <button class="mainbtts" onclick="move_page('kursy.php')" id="kursy" >Kursy</button>
    <button class="mainbtts" onclick="move_page('kursanci.php')" id="kursanci" >Kursanci</button>
    <button class="mainbtts" onclick="move_page('forum.php')" id="forum" >Forum</button>



    <br><br><br><br><br>
    <b><p style="font-family: Corbel; font-size: 20px; color: #333333;">O nas:</p></b>
    <b><p class="main"><span>Celem tego portalu jest, aby umożliwić Wam nieograniczony dostęp do wiedzy i umiejętności jaki zapewniają kompleksowe kursy doszkalające z zakresu informatyki.</span></p>
    <p class="main"><span>Oferujemy Wam możliwość odbywania kursów internetowych i stacjonarnych, pozyskiwania certyfikacji oraz zdobywania znajomości u wysokowykwalifikowanej kadry dydaktycznej.</span></p>
    <p class="main"><span>Działalność nasza jest skoncentrowana wyłącznie na kwestiach ściśle związanych z programowaniem, grafiką i obsługą urządzeń sieci. Dzięki temu jesteśmy stale dyspozycyjni i gotowi odpowiedzieć na każde wasze pytanie.</span></p></b>
    <br>
    <img src="images/Linia_w.png">

    <p><span>Oto lista najbliższych kursów:</span></p><br>

  <div style="position:relative; width: 1200px">
    <p style="position:relative; float:left; text-align: center; width: 400px">Programowanie</p>
    <p style="position:relative; float:left; text-align: center; width: 400px">Grafika</p>
    <p style="position:relative; float:left; text-align: center; width: 400px">Obsługa urządzeń sieci</p>
  </div>
  <br style="margin-bottom:8px;">

<div style="position: relative; left: 0px; width: 1200px">
  <div style="position: relative; height:60px;  float:left; width: 400px; ">
  <hr style="width:240px;">
  <div style="margin: 0 auto; width:200px">
    <p class="a">22.03.2019</p>
    <p class="a">Poznań</p>
    <p class="a">Stacjonarny</p>
    <p class="b" style="position: relative;  top:90%; transform: translateY(-50%);left:120px;top:-60px">Kurs C# - podstawy</p>
  </div>
  </div>

  <div style="position: relative; height:60px;  float:left; width: 400px">
  <hr style="width:240px;">
  <div style="margin: 0 auto; width:200px">
  <p class="a">28.03.2019</p>
  <p class="a">Warszawa</p>
  <p class="a">Stacjonarny</p>
  <p class="b" style="position: relative; top:90%; transform: translateY(-50%);left:120px;top:-60px">Kurs Adobe</p>
  </div>
  </div>

  <div style="position: relative; height:60px;  float:left; width: 400px">
  <hr style="width:240px;">
  <div style="margin: 0 auto; width:200px">
  <p class="a">30.03.2019</p>
  <p class="a">Kraków</p>
  <p class="a">Stacjonarny</p>
  <p class="b" style="position: relative; top:90%; transform: translateY(-50%);left:120px;top:-60px">Kurs C# - podstawy</p>
  </div>
  </div>
</div>

<div style="position:relative;top: 138px;">
<div style="position:relative;">
  <p><span style="font-family:'Calibri';font-weight:400;">Zarejestruj się aby móc również:</span></p><p><span style="font-family:'Calibri';font-weight:400;">&nbsp;&nbsp; &nbsp;&nbsp; - przeglądać listę wszystkich trwających i nadchodzących kursów</span></p>
  <p><span style="font-family:'Calibri';font-weight:400;">&nbsp;&nbsp; &nbsp;&nbsp; - przeglądać listę kursantów</span></p><p><span style="font-family:'Calibri';font-weight:400;">&nbsp;&nbsp; &nbsp;&nbsp; - przeglądać forum korespondencji z kursantami</span></p>
  <p><span style="font-family:'Calibri';font-weight:400;">&nbsp;&nbsp; &nbsp;&nbsp; - umieszczać wpisy na forum pomocy i opinii</span></p><p><span style="font-family:'Calibri';font-weight:400;">&nbsp;&nbsp; &nbsp;&nbsp; - </span><span style="font-family:'Calibri';font-weight:400;text-decoration:underline;">zarejestrować się na wybrany kurs</span></p>
  <p><span style="font-family:'Calibri';font-weight:400;"><br></span></p>
  <p><span style="font-family:'Calibri';font-weight:400;">Rejestracja nic nie kosztuje, nie ma również plików cookies, ani wiadomości reklam wysyłanych na skrzynkę pocztową. Po prostu zarejestruj się aby móc w pełni korzystać z portalu.</span></p>
  <p><span style="font-family:'Calibri';font-weight:400;">Aby to zrobić wejdź w zakłądkę </span><span style="font-family:'Calibri Bold', 'Calibri Regular', 'Calibri';font-weight:700;">Konto</span><span style="font-family:'Calibri';font-weight:400;"> i wykonaj rejestracje.</span></p>
  </div>
  <hr style="width:1200px;">
  <p><span>Najnowsze porozumnienia kursów certyfikowanych</span></p>
  
  <div style="height: 400px;width: 1200px;">

  <div style="position: relative; width: 400px; float:left">
  <div style="margin: 0 auto; width:200px">
    <p class="c"><span>CERT Company</span></p>
    <p class="c"><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>
    <p class="c"><span>Czytaj więcej</span></p>
  </div>
</div>

<div style="position: relative; width: 400px; float:left">
  <div style="margin: 0 auto; width:200px">
    <p class="c"><span>McCrew</span></p>
    <p class="c"><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>
    <p class="c"><span>Czytaj więcej</span></p>
  </div>
</div>

<div style="position: relative; width: 400px; float:left">
  <div style="margin: 0 auto; width:200px">
    <p class="c"><span>Cretyfikator</span></p>
    <p class="c"><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>
    <p class="c"><span>Czytaj więcej</span></p>
    </div>
  </div>
</div>

<div>
  <hr style="width:1200px; ">
  
  <p><span>Nowe wpisy na forum</span></p>
</div>
  <div style="height:500px">
  <div style="position: absolute; clip: rect(0px 1200px 4000px 10px); width:1200px; height:400px">

  <div class="dyn1" style="animation-delay: 0s;">
    <img style="float:left" class="img2" src="images/user1.png">
    <p style="position: relative; width:400; height:400" ><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>
  </div>

  <div class="dyn1" style="animation-delay: 3s;">
    <img style="float:left" class="img2" src="images/user2.png">
    <p style="position: relative; width:400; height:400" ><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>
  </div>
  </div>
</div>
</div>
<hr style="width:1200px;">

<p><span>Nasi partnerzy</span></p>
<div style="width:1200px; height: 400px">
  <div style="position: relative; width: 400px; height: 400px; float:left">
  <img class="img2" style="position: relative; top: 50%; display: block; margin: 0 auto; transform: translateY(-50%); " src="images/u40.png">
  </div>
  <div style="position: relative; width: 400px; height: 400px; float:left">
  <img class="img2" style="position: relative; top: 50%; display: block; margin: 0 auto; transform: translateY(-50%); " src="images/u41.jpg">
  </div>
  <div style="position: relative; width: 400px; height: 400px; float:left">
  <img class="img2" style="position: relative; top: 50%; display: block; margin: 0 auto; transform: translateY(-50%); " src="images/u42.png">
  </div>
</div>
<hr style="width:1200px; border-top: 1px solid black;">
<div style="width:1202px; height: 16px; position:relative;">
    <p style="text-align: center; font-size:13px; color:black; position:relative; margin-bottom:0px"><span>admin@example.com <b>2019</b></span></p>
  <div style="position: absolute; width:1202px; height: 40px; background: #999; top:-10px; left: -1px; z-index: -2; margin-bottom:0px">
   </div>
  <?php
 //left: -0px; top: 0px;
  ?>
  </div>
  
  </div>
</div>
</body>
