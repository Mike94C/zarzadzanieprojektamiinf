<!DOCTYPE html>
<html>
  <head>
    <title>Szczegóły mojego</title>
    <meta charset="UTF-8">
    <link href="mainstyles.css" type="text/css" rel="stylesheet"/>
    <style>

    </style>
      <script>
    function move_page(a){
      location.href = a;
    }
  </script>
</head>
<body>

<div id="main_div"> 
  <div style="width: 1200px; margin: 0 auto" >
    <button>Strona główna</button>
    <button id="konto">Konto</button>
    <br>
    <img class="img1" src="images/Baner.png">
    <br>
    <button class="mainbtts" onclick="move_page('kursy.php')" id="kursy" >Kursy</button>
    <button class="mainbtts" onclick="move_page('kursanci.php')" id="kursanci" >Kursanci</button>
    <button class="mainbtts" onclick="move_page('forum.php')" id="forum" >Forum</button>
    <br><br><br><br>

    <p class="d"><span>Szczegóły kursu</span></p>
    <p class="e" style="width: 440px"><span>Tutaj znajdziesz pełną informacje nt. kursy który odbywasz. Oceny, obecności, punkty i opinie kursanta są w jednym widoku.</span></p><br>
    <p style="font-size:20px"><span>Kurs prowadzony przez:</span><span> --prowadzący--</span><br><br><br><br><span>Tabela obecności:</span></p><br>

    <table style="width:880px;">
    <tr >
        <th class="det">Zajęcia</th>
        <th class="det">Obecność</th>
        <th class="det">Punkty</th>
        <th class="det">Maks. punktów</th>
        <th class="det">Procent</th>
    </tr>
    <tr>
        <td class="det">Lekcja 1</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    <tr>
        <td class="det">Lekcja 2</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    <tr>
        <td class="det">Lekcja 3</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    <tr>
        <td class="det">Lekcja 4</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    <tr>
        <td class="det">Lekcja 5</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    <tr>
        <td class="det">Lekcja 6</td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
        <td class="det"></td>
    </tr>
    </table><br><br>
    <hr>
    <p><span>Opinie kursanta:</span></p>
    <div class="l" style="height: 400px">
        <div style="position: relative; top: 200px">
            <p style="text-align:center; width:1200px; font-size:28px; font-weight: 200; "><span>Brak opini</span></p><p style="text-align:center; width:1200px; font-size:16px; font-weight: 600; "><span>Nie dodano nic dotychczas</span></p>
        </div>
    </div>
    </div><!-- Strona -->
</div><!-- main_div -->
</body>