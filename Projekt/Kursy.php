<!DOCTYPE html>
<html>
  <head>
    <title>Kursy</title>
    <meta charset="UTF-8">
    <link href="mainstyles.css" type="text/css" rel="stylesheet"/>
</head>
<script type="text/javascript">

  var proginfo;
  var grafinfo;
  var siecinfo;
  var mcdiv;
  function changeColor(){
  //mcdiv = document.getElementById("divt");
  //proginfo = document.getElementById("proginfo");
  //proginfo.style.visibility = "visible";
  //alert(mcdiv.style.height);
    //mcdiv.style.visibility = "hidden";
    //mcdiv.style.height = "20px";
  }

  function findInfo(){
    mcdiv = document.getElementById("divt");
    proginfo = document.getElementById("proginfo");
    grafinfo = document.getElementById("grafinfo");
    siecinfo = document.getElementById("siecinfo");
  }

  function selekcja(param){
    if(param=='p'){
      mcdiv.style.top = (proginfo.getAttribute("data-type")+"px");
    proginfo.style.visibility = "visible";
    grafinfo.style.visibility = "hidden";
    siecinfo.style.visibility = "hidden";
    }
    if(param=='g'){
      mcdiv.style.top = (grafinfo.getAttribute("data-type")+"px");
    proginfo.style.visibility = "hidden";
    grafinfo.style.visibility = "visible";
    siecinfo.style.visibility = "hidden";
    }
    if(param=='s'){
      mcdiv.style.top = (siecinfo.getAttribute("data-type")+"px");
    proginfo.style.visibility = "hidden";
    grafinfo.style.visibility = "hidden";
    siecinfo.style.visibility = "visible";
    }
  }

  function kurs_move(a){
    var Tytul_ = "Tytul_"+a;
    var Rozp_ = "Rozp_"+a;
    var Zako_ = "Zako_"+a;
    var Licz_ = "Licz_"+a;
    var Loka_ = "Loka_"+a;
    var Cena_ = "Cena_"+a;
    var Cert_ = "Cert_"+a;
    var Opis_ = "Opis_"+a;
    var Tytul_x = document.getElementById(Tytul_).innerHTML;
    var Rozp_x = document.getElementById(Rozp_).innerHTML;
    var Zako_x = document.getElementById(Zako_).innerHTML;
    var Licz_x = document.getElementById(Licz_).innerHTML;
    var Loka_x = document.getElementById(Loka_).innerHTML;
    var Cena_x = document.getElementById(Cena_).innerHTML;
    var Cert_x = document.getElementById(Cert_).innerHTML;
    var Opis_x = document.getElementById(Opis_).innerHTML;

    sessionStorage.setItem('Tytul_x', Tytul_x);
    sessionStorage.setItem('Rozp_x', Rozp_x);
    sessionStorage.setItem('Zako_x', Zako_x);
    sessionStorage.setItem('Licz_x', Licz_x);
    sessionStorage.setItem('Loka_x', Loka_x);
    sessionStorage.setItem('Cena_x', Cena_x);
    sessionStorage.setItem('Cert_x', Cert_x);
    sessionStorage.setItem('Opis_x', Opis_x);
    location.href="Kursy_szczegoly.php";
  }


    function move_page(a){
      location.href = a;
    }

</script>
<body onload="changeColor(), findInfo()">
<div id="main_div"> 
  <div style="width: 1200px; margin: 0 auto" >
    <button>Strona główna</button>
    <?php
    session_cache_limiter('');
    session_start();
      if (!isset($_SESSION['login'])) {
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">Konto</button>';
      } else {
          echo '<button id="wyloguj" onclick="move_page'."('Wyloguj.php')".'">Wyloguj</button>';
          echo '<button id="konto" onclick="move_page'."('Konto.php')".'">'.$_SESSION['login'].'</button>';
      }
    ?>

    <br>
    <img class="img1" src="images/Baner.png">
    <br>
    <button class="mainbtts" onclick="move_page('kursy.php')" id="kursy" >Kursy</button>
    <button class="mainbtts" onclick="move_page('kursanci.php')" id="kursanci" >Kursanci</button>
    <button class="mainbtts" onclick="move_page('forum.php')" id="forum" >Forum</button>
    <br><br><br><br>
    <p class="d"><span>Obecnie dostępne kursy</span></p>
    <p class="e" style="width: 600px"><span>Wyświetlone są tutaj wszystkie dostępne kursy. Możesz się na nie zapisać, ale tylko wtedy gdy jesteś zalogowany. Aby to zrobić kliknij z zakładkę "Konto" w prawym górnym rogu.</span></p>
    <div class="l" style="height: 120px">
      <div class="m">
      <button onclick='selekcja("p");' class="main" style="">Prograrmowanie</button>
      </div>
      <div class="m">
      <button onclick='selekcja("g");' class="main" style="">Grafika</button>
      </div>
      <div class="m">
      <button onclick='selekcja("s");' class="main" style="">Obsługa urządzeń sieci</button>
      </div>
    </div>
    <?php
    //kurs_move('.$linia['ID_Kursu'].')
    session_cache_limiter('');
    session_start();

    //echo date('l', strtotime('2012-10-15'));
    $polaczenie = new mysqli('localhost', 'root', '', 'mc');
    $liczba = $polaczenie->query('SELECT COUNT( *) as "liczba" FROM kursy;');
    $linia = $liczba->fetch_assoc();
    $liczba = $linia['liczba'] / 3;
    $l = ceil($liczba);

    $h = $l * 440;
    echo '<div id="divinfo" class="l" style="height:0px">';

      $wynik = $polaczenie->query('SELECT ID_Kursu, Nazwa, Rodzaj, Rozpoczecie, Zakonczenie, Liczba_godzin, Lokalizacja, Cena, Certyfikacja, Prowadzacy_imie, Opis FROM kursy');
      $prog = '';
      $progit = 0;
      $graf = '';
      $grafit = 0;
      $siec = '';
      $siecit = 0;

      while (($linia = $wynik->fetch_assoc()) !== null) {
          if ($linia['Rodzaj'] === 'p') {
              $prog .= '<div class="m1"> <h3 id="Tytul_'.$linia['ID_Kursu'].'">'.$linia['Nazwa'].'</h3> <h3 class="example" style="float:right; visibility: hidden" id="Wartosc_'.$linia['ID_Kursu'].'"> <p class="m1" ><span style="float: left">Rozpoczęcie:</span></p><p class="m1" ><span id="Rozp_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Rozpoczecie'].'</span></p>'.$linia['Rodzaj'].'</h3> <p class="m1" ><span style="float: left">Zakończenie:</span></p><p class="m1" ><span id="Zako_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Zakonczenie'].'</span></p> <p class="m1" ><span style="float: left">Liczba godzin:</span></p><p class="m1" ><span id="Licz_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Liczba_godzin'].'</span></p> <p class="m1" ><span style="float: left">Lokalizacja:</span></p><p class="m1" ><span id="Loka_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Lokalizacja'].'</span></p> <p class="m1" ><span style="float: left">Cena:</span></p><p class="m1" ><span id="Cena_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Cena'].'</span></p> <p class="m1" ><span style="float: left">Certyfikacja:</span></p><p class="m1" ><span id="Cert_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Certyfikacja'].'</span></p> <form method="POST" action="Kursy_szczegoly.php"> <input name="kurs_id" type="hidden" value="'.$linia['ID_Kursu'].'"> <input name="name" type="hidden" value="'.$linia['Nazwa'].'"> <input class="p" type="submit" onclick="kurs_move('.$linia['ID_Kursu'].')" style="margin-right: 10px; font-size: 14px" value="Więcej"> </form> <p class="m1" ><span>Opis:</span></p><p style="font-size: 13px; text-align: left" ><span id="Opis_'.$linia['ID_Kursu'].'">'.$linia['Opis'].'</span></p> </div>';
              ++$progit;
          }
          if ($linia['Rodzaj'] === 'g') {
              $graf .= '<div class="m1"> <h3 id="Tytul_'.$linia['ID_Kursu'].'">'.$linia['Nazwa'].'</h3> <h3 class="example" style="float:right; visibility: hidden" id="Wartosc_'.$linia['ID_Kursu'].'"> <p class="m1" ><span style="float: left">Rozpoczęcie:</span></p><p class="m1" ><span id="Rozp_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Rozpoczecie'].'</span></p>'.$linia['Rodzaj'].'</h3> <p class="m1" ><span style="float: left">Zakończenie:</span></p><p class="m1" ><span id="Zako_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Zakonczenie'].'</span></p> <p class="m1" ><span style="float: left">Liczba godzin:</span></p><p class="m1" ><span id="Licz_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Liczba_godzin'].'</span></p> <p class="m1" ><span style="float: left">Lokalizacja:</span></p><p class="m1" ><span id="Loka_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Lokalizacja'].'</span></p> <p class="m1" ><span style="float: left">Cena:</span></p><p class="m1" ><span id="Cena_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Cena'].'</span></p> <p class="m1" ><span style="float: left">Certyfikacja:</span></p><p class="m1" ><span id="Cert_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Certyfikacja'].'</span></p> <form method="POST" action="Kursy_szczegoly.php"> <input name="kurs_id" type="hidden" value="'.$linia['ID_Kursu'].'"> <input name="name" type="hidden" value="'.$linia['Nazwa'].'"> <input class="p" type="submit" onclick="kurs_move('.$linia['ID_Kursu'].')" style="margin-right: 10px; font-size: 14px" value="Więcej"> </form> <p class="m1" ><span>Opis:</span></p><p style="font-size: 13px; text-align: left" ><span id="Opis_'.$linia['ID_Kursu'].'">'.$linia['Opis'].'</span></p> </div>';
              ++$grafit;
          }
          if ($linia['Rodzaj'] === 's') {
              $siec .= '<div class="m1"> <h3 id="Tytul_'.$linia['ID_Kursu'].'">'.$linia['Nazwa'].'</h3> <h3 class="example" style="float:right; visibility: hidden" id="Wartosc_'.$linia['ID_Kursu'].'"> <p class="m1" ><span style="float: left">Rozpoczęcie:</span></p><p class="m1" ><span id="Rozp_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Rozpoczecie'].'</span></p>'.$linia['Rodzaj'].'</h3> <p class="m1" ><span style="float: left">Zakończenie:</span></p><p class="m1" ><span id="Zako_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Zakonczenie'].'</span></p> <p class="m1" ><span style="float: left">Liczba godzin:</span></p><p class="m1" ><span id="Licz_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Liczba_godzin'].'</span></p> <p class="m1" ><span style="float: left">Lokalizacja:</span></p><p class="m1" ><span id="Loka_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Lokalizacja'].'</span></p> <p class="m1" ><span style="float: left">Cena:</span></p><p class="m1" ><span id="Cena_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Cena'].'</span></p> <p class="m1" ><span style="float: left">Certyfikacja:</span></p><p class="m1" ><span id="Cert_'.$linia['ID_Kursu'].'" style="margin-left: 6px">'.$linia['Certyfikacja'].'</span></p> <form method="POST" action="Kursy_szczegoly.php"> <input name="kurs_id" type="hidden" value="'.$linia['ID_Kursu'].'"> <input name="name" type="hidden" value="'.$linia['Nazwa'].'"> <input class="p" type="submit" onclick="kurs_move('.$linia['ID_Kursu'].')" style="margin-right: 10px; font-size: 14px" value="Więcej"> </form> <p class="m1" ><span>Opis:</span></p><p style="font-size: 13px; text-align: left" ><span id="Opis_'.$linia['ID_Kursu'].'">'.$linia['Opis'].'</span></p> </div>';
              ++$siecit;
          }
      }
      $proghei = ceil($progit / 3) * 440;
      echo '<div data-type="'.$proghei.'" id="proginfo" style="visibility: hidden; position: absolute; height: '.$proghei.'px; width: 1200px">';
      echo $prog.'</div>';
      $grafhei = ceil($grafit / 3) * 440;
      echo '<div data-type="'.$grafhei.'" id="grafinfo" style="visibility: hidden; position: absolute; height: '.$grafhei.'px; width: 1200px">';
      echo $graf.'</div>';
      $siechei = ceil($siecit / 3) * 440;
      echo '<div data-type="'.$siechei.'" id="siecinfo" style="visibility: hidden; position: absolute; height: '.$siechei.'px; width: 1200px">';
      echo $siec.'</div>';

    ?>
        <!--<div style="position: absolute; background: #333; height: 400px; width: 1200px"></div>-->
    </div>
    <div id="divt" style="position: relative; left: 0px; top: 0px; " >
      <hr style="width: 1200px">
      <p id="idp" class="m"><span>Czy brakuje interesującej propozycji?</span></p>
      <p class="s" style="height:120px"><span>Jeśli tak to wejdź na forum i opisz swoją propozycję. Istnieje spora szansa, że kursanci, przy dużym zainteresowaniu osób takich jak Ty, zaaprobują twoją prośbę i pojawi się tu owa oferta. Będziemy Ci wdzęczni za twój wkład w rozwój naszego portalu.</span></p>
      <br>
      <hr style="width: 1200px">
      <p class="m"><span>Czy masz dla nas propozycje współpracy?</span></p>
      <p class="s" style="height:120px"><span>Bardzo chętnie przyjmiemy nowych kursantów do naszego grona. Zarejestruj się jako kursant w zakładce "Konto" w prawym górnym rogu. Odpiszemy w przeciągu kilku godzin.</span></p>
      <br>
      <hr style="width: 1200px">
      <p class="m"><span>Czy masz pytania do nas?</span></p>
      <p class="s" style="height:240px"><span>Odpowiemy na nie. Mail do nas to admin@example.com. </span></p>
        <div style="height: 40px; background: #999;">
          <hr style="width:1200px; border-top: 1px solid black;">
          <div style="width:1202px; height: 16px; position:relative;">
          <p style="text-align: center; font-size:13px; color:black; position:relative; margin-bottom:0px"><span>admin@example.com <b>2019</b></span></p>
        </div>
     </div>
    
</div>
    </div><!-- Strona -->
</div><!-- main_div -->
</body>